import { Injectable } from '@nestjs/common';
import { Course } from './interface/course.interface'

@Injectable()
export class CoursesService{
    async findAll(): Promise<Course[]>{
        return [
            {
              id:'100',
              number:'01204111',
              title:'Computer abd Programing'
            },
            {
              id:'200',
              number:'05102315',
              title:'Discrete Mathematic'
            },
            {
              id:'300',
              number:'32151021',
              title:'Design Algorithm'
            },
          ];
    }
}