import React, { useEffect, useState } from 'react';
import './App.css';

const App = () => {
  const [courses, setCourses] = useState<any[]>([]);

  useEffect(() => {
    fetch('http://localhost:3000/courses')
    .then(res => res.json())
    .then(courses =>{
      console.log(courses)
      setCourses(courses);  
    });
  },[]);

  return(
    <div className="App">
      {courses.map((item) => {return item.title; })}
    </div>
  );
}

/*
type AppState = {
  message: string;
};

class App extends React.Component<{}, AppState>{
  state: AppState = {
    message: 'Default messages',
  };

  componentDidMount(){
    fetch('http://localhost:3000/courses')
      .then(res => res.json())
      .then(obj =>{
        this.setState({message: obj.message});
      })
  }
  render() {
    return (
      <div>
        {this.state.message}
      </div>
    );
  }
}
*/
export default App;
